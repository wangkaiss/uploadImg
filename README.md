# uploadImg

#### 项目介绍
eclipse+maven+ssm+mysql 上传文件简单演示项目

#### 软件架构
eclipse+maven+ssm+mysql+tomcat


#### 使用说明

1. eclipse导入项目后如果出现项目名上有红叉，鼠标右键选中项目名,然后选中maven，然后选择update project,
2. 数据库配置文件自行修改
3. 每次重启tomcat后，上传的图片显示不了了，因为你的项目又重新发布了，覆盖了以前的，要想 解决此问题，推荐在tomcat的配置文件中设置一个用来放置图片的虚拟目录，具体操作请自行百度，这样就不会覆盖了

![输入图片说明](https://images.gitee.com/uploads/images/2018/1001/141523_4a1c41b6_2054011.png "屏幕截图.png")